<!--
.. title: Donations
.. slug: donate
.. date: 2019-01-20 10:30:39 UTC+03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. pretty_url: False
-->

Imaginario is developed in my spare time. I do enjoy working on it, but at the
same time I'm also working on other projects (including
[PhotoTeleport](http://phototeleport.com), [Ubports](http://ubports.com)),
hobbies ([photography](http://photo.mardy.it), of course!), [studying
Russian](https://mardy.livejournal.com/) and, last but definitely not least,
spending quality time with my wife and kids.

It's easy for me to lose motivation, or to “freeze” development of a project
for a few months while I play with something else. To help avoiding this
catastrophe, please consider making a donation: this is the strongest signal
you can send me about you wanting me to continue working on Imaginario.


Using PayPal
------------

It couldn't be simpler: just click on [this
link](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=SUEP2EDA8VMRA)
and follow the instructions.


Using Yandex
-------------------

Yandex's donation form (if you see a “₽” symbol in the amount field, please
[use a currency
converter](https://www.xe.com/currencyconverter/convert/?Amount=1&From=USD&To=RUB)
and enter the amount in Russian rubles):

<iframe
src="https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=Imaginario&targets-hint=&default-sum=&button-text=14&payment-type-choice=on&comment=on&hint=&successURL=&quickpay=shop&account=410015419471966"
width="423" height="301" frameborder="0"
allowtransparency="true" scrolling="no"></iframe>
