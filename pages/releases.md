<!--
.. title: Releases
.. slug: releases
.. date: 2019-01-18 18:34:02 UTC+03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. pretty_url: False
-->

Latest downloads
================

Here's the list of the most recent Imaginario releases; please make sure to
choose the right platform for your computer.

**NOTE**: Currently, Imaginario is in alpha stage. All major releases will
available for Windows, MacOS and Linux, but more frequent development releases
will happen for Linux only.

<ul>
{{% template %}}
{% for version_data in global_data.releases %}
  <li>Version <b>{{version_data.version}}</b>:
  {% set packages = version_data.packages %}
  {% for platform,file in packages.items() %}
  {% set plat_data = global_data.platforms[platform] %}
  <a href="/releases/Imaginario_{{file}}.{{ plat_data.extension }}">{{plat_data.name}}</a>
  {% if not loop.last %} &mdash; {% endif %}
  {% endfor %}
{% endfor %}
{{% /template %}}
</ul>

---

**Please consider supporting Imaginario** with a
[donation](link://slug/donate): it will keep me motivated and help me dedicate
more time to its development.

Reviewing it in your blog, or leaving a positive review for it in the [Apple
store](https://apps.apple.com/us/app/imaginario/id1484961672) will also be of
enormous help.
