<!---
.. title: Privacy Policy
.. slug: privacy_policy
.. type: text
.. pretty_url: False
-->

Imaginario respects your privacy: all the content imported into Imaginario is
not sent over any network connection.

Imaginario will not try to access any files belonging to the user, except for
image files stored in the user's Picture directory and for those files which
the user has explicitly imported into Imaginario.

Such files will be processed like follows:

1. Unless the user has chosen to store the pictures metadata inside the image
   files themselves, the user files will not be modified in any way.

2. Photo metadata will be stored inside the user image files only if the user
   has requested this behaviour from the Preferences screen.

3. The files will not be shared with any other application unless the user has
   explicitly requested to open them in another application.

The user is at any time free to move to another application or device all the
content handled by Imaginario.
