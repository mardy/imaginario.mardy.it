.. title: Frequently Asked Questions
.. slug: faq
.. date: 2019-01-19 18:23:14 UTC+03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. pretty_url: False

.. contents:: Index of questions


Where do I...?
--------------

- ...get the application? → `Releases page <link://slug/releases>`_
- ...find the source code? → `gitlab.com/mardy/imaginario
  <https://gitlab.com/mardy/imaginario>`_
- ...file a bug or feature request? → `gitlab.com/mardy/imaginario/issues
  <https://gitlab.com/mardy/imaginario/issues>`_
- ...get the latest news? → `Mardy's blog
  <http://www.mardy.it/categories/imaginario.html>`_

How, and especially *why* was this project born?
------------------------------------------------

We didn't really need another photo manager, did we? There are already plenty
of excellent applications for managing your photos, I have to admit.

But how many photo managers do we have in `Ubports`_? Indeed, Imaginario was
born as a `mobile application <http://www.mardy.it/imaginario/>`_ and only
once that was completed and proved to be working `I started working on a
desktop version
<http://blog.mardy.it/2016/05/imaginario-for-your-desktop.html>`_.


Why does Imaginario resemble F-Spot so much?
--------------------------------------------

Because its main developer was a fan of `F-Spot`_ and of its simple and
efficient user interface. As F-Spot ceased to be actively developed, he started
looking around for a replacement, but couldn't find an alternative that would
make him feel completely *at home*.


But F-Spot *is* still being developed!
--------------------------------------

I know! And it's great news! :-) Some people have resumed working on F-Spot in
`github <https://github.com/f-spot/f-spot>`_, but there was a time (in 2016
and 2017) when the project was completely dead; that is what gave me the
incentive to work on a desktop version of Imaginario, and use F-Spot as a
reference for the user interface.


What are Imaginario's selling points?
-------------------------------------

In one word: simplicity. Its goal is to enable you to store and categorize
your photos in a way that allows for an easy and fast lookup later on. It does
not try to integrate raw file processing, photo editing or photo sharing into
the same app, but it rather lets you choose your favourite tools and plays
nicely with them. The best programs for the job, for instance the `Gimp`_
image editor and the `PhotoTeleport`_ photo uploader, are preconfigured into
Imaginario list of external tools, but the list is fully customizable
according to your preferences.


Can I import my photo database from other applications?
-------------------------------------------------------

At the moment, Imaginario supports importing your photo database and settings
from `F-Spot`_ and `Shotwell
<https://wiki.gnome.org/Apps/Shotwell>`_. If you are using another photo
manager which is open source, or whose database format is openly documented,
feel free to `file a request <https://gitlab.com/mardy/imaginario/issues>`_
to have Imaginario import your photo collection from it.


How much does it cost?
----------------------

Imaginario is Free and Open Source Software (FOSS), meaning that everyone is
free to redistribute it, for free or for a fee, provided that the source code
for any modifications is also distributed. In practice, you can download
Imaginario for free from this website, and the plan is to make it also
available for purchase in the Apple and Windows stores.


How can I support Imaginario's development?
-------------------------------------------

You can contribute code, bug reports, new ideas or translations at
`the project development site <https://gitlab.com/mardy/imaginario>`_, or you
can `contribute with a donation here <link://slug/donate>`_.

Another good way to help the project is just to spread the word about it.


Is it ready for use?
--------------------

The desktop version of Imaginario is still a beta version; there's still a
risk of losing or damaging your photos. Of course, every version is being
smoke tested before release, but given that at the moment the development team
consists of just one person, it's impossible to verify all the different
settings and configurations that one might be using.


Why there's no Windows or macOS version for download?
-----------------------------------------------------

Because we are still working on it. Stay tuned :-)


.. _F-Spot: https://en.wikipedia.org/wiki/F-Spot
.. _Ubports: https://ubports.com
.. _Gimp: https://www.gimp.org
.. _PhotoTeleport: https://phototeleport.com

