<!--
.. title: Imaginario 0.10 is out!
.. slug: imaginario-010-is-out
.. date: 2020-11-12 18:16:28 UTC+03:00
.. tags: release
.. category: 
.. link: 
.. description: 
.. type: text
-->

A new release of Imaginario is available! No new features were added in this
version, but a couple of important bugs have been fixed:

- Pending background jobs would not be resumed on the next startup
- The application no longer crashes when it's quit while some background
  operations are in progress

Users are recommended to [upgrade to Imaginario 0.10](link://slug/releases)!
