<!--
.. title: Face tagging with Imaginario 0.9
.. slug: face-tagging-with-imaginario-09
.. date: 2020-03-09 09:50:58 UTC+03:00
.. tags: release
.. category: 
.. link: 
.. description: 
.. type: text
-->

Tagging faces has never been faster! The newly released version 0.9 allows you
to create face tags directly from the images grid overview. [See it in
action!](https://youtu.be/kfEuEPaOYUY)
<!-- TEASER_END -->

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/kfEuEPaOYUY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>

This new functionality can be used to tag people's faces, as well as other
objects which you find of special importance. Tag information will be written
in the same fashion as for ordinary tags: either embedded in the image
medadata, in a sidecar XMP file, or only in the Imaginario database, depending
on how you've configured the application.

Visibility of the tag areas from the overview can be toggled on and off.

While the above is indeed the main feature coming with Imaginario 0.9, work has
continued in other areas as well:

- Fixed a crashing bug when two tags where deleted in a row
- Added a status bar to show hints and contextual help
- Improve handling of shortcuts: `t` and `f` will now always work, regardless
  of the focused item; also, keyboard navigation has been improved a bit
  everywhere
- Corrected the layout of the tag editing and deletion dialog
- Fix a crash occurring when closing the import dialog while importing

With the usual disclaimer, which **I make no guarantees are made about the
safety of your photos**, I warmly invite you to
[download](link://slug/releases) it and try it out!
