<!--
.. title: Version 0.8 has been released!
.. slug: version-08
.. date: 2019-12-02 22:53:43 UTC+03:00
.. tags: release
.. category: 
.. link: 
.. description: 
.. type: text
-->

Version 0.8, which since a few weeks has been available in the [Apple
store](https://apps.apple.com/us/app/imaginario/id1484961672), has now been
released for Linux and Windows as well.
<!-- TEASER_END -->

This release contains:

- fixes to the database access (which could fail in some situations),
- support for HEIC and HEIF files in macOS,
- support [Mappero Geotagger](http://www.mardy.it/mappero-geotagger/) as an external editor,
- a new "Check for updates" dialog,
- a new dialog to select external applications,
- many fixes to the import dialog,
- improved speed when deleting many photos,

and many more minor fixes. All that notwithstanding, Imaginario should still be
considered *alpha quality*: **no guarantees are made about the safety of your
photos** — none whatsoever!

But if you still want to try it, you are welcome to download it from the
[Releases page](/releases.html).
